# Events or Interface Listeners

So you want to know the difference between events and interface listeners? You are in a right place my friend! In this repository I'm showing example of handling user input with events and with interface listeners.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/05/02/ui-events-or-interfaces/

Enjoy!

---

# How to use it?

This repository contains an example of using events and listeners within UI in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/events-or-interfaces/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned the difference between handling with events and interface listeners!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
