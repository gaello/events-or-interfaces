﻿using UnityEngine;

/// <summary>
/// Example of handling UI with events.
/// </summary>
public class EventHandlingExample : MonoBehaviour
{
    // Reference to the view with events.
    [SerializeField]
    private UIEventView eventView;

    /// <summary>
    /// Unity method called when component is enabled.
    /// Here used to attach events.
    /// </summary>
    private void OnEnable()
    {
        eventView.OnPlayClicked += OnPlayClicked;
        eventView.OnOptionsClicked += OnOptionsClicked;
        eventView.OnQuitClicked += OnQuitClicked;
    }

    /// <summary>
    /// Unity method called when component is disabled.
    /// Here used to detach events.
    /// </summary>
    private void OnDisable()
    {
        eventView.OnPlayClicked -= OnPlayClicked;
        eventView.OnOptionsClicked -= OnOptionsClicked;
        eventView.OnQuitClicked -= OnQuitClicked;
    }

    /// <summary>
    /// Handling UI Play Button Click.
    /// </summary>
    private void OnPlayClicked()
    {
        Debug.Log("[Event] Play clicked");
    }

    /// <summary>
    /// Handling UI Options Button Click.
    /// </summary>
    private void OnOptionsClicked()
    {
        Debug.Log("[Event] Options clicked");
    }

    /// <summary>
    /// Handling UI Quit Button Click.
    /// </summary>
    private void OnQuitClicked()
    {
        Debug.Log("[Event] Quit clicked");
    }
}
