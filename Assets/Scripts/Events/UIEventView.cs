﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Example of view based on events/actions.
/// </summary>
public class UIEventView : MonoBehaviour
{
    // Event called when Play Button is clicked.
    public UnityAction OnPlayClicked;

    /// <summary>
    /// Method called by Play Button.
    /// </summary>
    public void PlayClicked()
    {
        OnPlayClicked?.Invoke();
    }

    // Event called when Options Button is clicked.
    public UnityAction OnOptionsClicked;

    /// <summary>
    /// Method called by Options Button.
    /// </summary>
    public void OptionsClicked()
    {
        OnOptionsClicked?.Invoke();
    }

    // Event called when Quit Button is clicked.
    public UnityAction OnQuitClicked;

    /// <summary>
    /// Method called by Quit Button.
    /// </summary>
    public void QuitClicked()
    {
        OnQuitClicked?.Invoke();
    }
}
