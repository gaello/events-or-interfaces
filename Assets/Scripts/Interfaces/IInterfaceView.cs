﻿/// <summary>
/// Interface for UIInterfaceView.
/// </summary>
public interface IInterfaceView
{
    // Play Button clicked.
    void OnPlayClicked();

    // Options Button clicked.
    void OnOptionsClicked();

    // Quit Button clicked.
    void OnQuitClicked();
}
