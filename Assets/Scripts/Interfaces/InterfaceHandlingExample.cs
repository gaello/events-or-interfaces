﻿using UnityEngine;

/// <summary>
/// Example of handling UI with interface listener.
/// </summary>
public class InterfaceHandlingExample : MonoBehaviour, IInterfaceView
{
    // Reference to the view with interface listener.
    [SerializeField]
    private UIInterfaceView interfaceView;

    /// <summary>
    /// Unity method called when component is enabled.
    /// Here used to assign listener in view.
    /// </summary>
    private void OnEnable()
    {
        interfaceView.listener = this;
    }

    /// <summary>
    /// Unity method called when component is disabled.
    /// Here used to remove reference from view.
    /// </summary>
    private void OnDisable()
    {
        interfaceView.listener = null;
    }

    /// <summary>
    /// Handling UI Play Button Click.
    /// </summary>
    public void OnPlayClicked()
    {
        Debug.Log("[Interface] Play clicked");
    }

    /// <summary>
    /// Handling UI Options Button Click.
    /// </summary>
    public void OnOptionsClicked()
    {
        Debug.Log("[Interface] Options clicked");
    }

    /// <summary>
    /// Handling UI Quit Button Click.
    /// </summary>
    public void OnQuitClicked()
    {
        Debug.Log("[Interface] Quit clicked");
    }
}
