﻿using UnityEngine;

/// <summary>
/// Example of view based on interface listener.
/// </summary>
public class UIInterfaceView : MonoBehaviour
{
    // Reference to listener.
    public IInterfaceView listener;

    /// <summary>
    /// Method called by Play Button.
    /// </summary>
    public void PlayClicked()
    {
        listener?.OnPlayClicked();
    }

    /// <summary>
    /// Method called by Options Button.
    /// </summary>
    public void OptionsClicked()
    {
        listener?.OnOptionsClicked();
    }

    /// <summary>
    /// Method called by Quit Button.
    /// </summary>
    public void QuitClicked()
    {
        listener?.OnQuitClicked();
    }
}
